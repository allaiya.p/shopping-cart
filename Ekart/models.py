from django.db import models
from django.contrib.auth.models import User

# Create your models here.



class Products(models.Model):
    title = models.CharField(max_length=75)
    description = models.CharField(max_length=300)
    price = models.DecimalField(max_digits=10,decimal_places=4)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
       return self.title


class Orders(models.Model):
    payment_choices = (
        ('cash', 'Cash'),
        ('paytm', 'Paytm'),
        ('card', 'Card'),
    )

    status_choices=(
        ('new','New'),
        ('paid','Paid')

    )

    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    total = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=20,choices=status_choices)
    node_of_payments = models.CharField(max_length=30, choices=payment_choices)

    def __str__(self):
        return str(self.id)


class OrdersItems(models.Model):
    order_id = models.ForeignKey(Orders, on_delete=models.CASCADE)
    product_id = models.ForeignKey(Products, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=4)

    def __str__(self):
        return str(self.id)


