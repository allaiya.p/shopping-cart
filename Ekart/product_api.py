from Ekart.models import Products
from rest_framework import viewsets,permissions
from Ekart.serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    permission_classes =[
        permissions.AllowAny
    ]
    serializer_class = ProductSerializer