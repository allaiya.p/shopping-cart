from rest_framework import routers
from .product_api import ProductViewSet
from .order_api import OrderViewSet


router = routers.DefaultRouter()
router.register('rest_api/products',ProductViewSet,'Ekart')
router.register('rest_api/orders',OrderViewSet,'Ekart')


urlpatterns = router.urls