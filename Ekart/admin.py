from django.contrib import admin
from Ekart.models import Products,Orders,OrdersItems

# Register your models here.

admin.site.register(Products)
admin.site.register(Orders)
admin.site.register(OrdersItems)
